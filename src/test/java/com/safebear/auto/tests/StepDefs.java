package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class StepDefs {

    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;

   @Before()
    public void setUp() {
        driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);

    }

    @After
    public void tearDown() {
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "500")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }

    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() throws Throwable {
        driver.get(Utils.getUrl());
  //      Assert.assertTrue(loginPage.isPageTitleAsExpected());
        Assert.assertEquals(loginPage.getExpectedPageTitle(), loginPage.getPageTitle(), "We're not on the correct page");
    }

    @When("^I enter the login info for '(.+)'$")
    public void i_enter_the_login_info_for_a_user(String user) throws Throwable {
        switch(user){
            case "invalidUser":
                loginPage.login("badtester","letmein");
                break;
            case "validUser":
                loginPage.login("tester","letmein");
                break;
            default:
                Assert.fail("User test data is invalid or not available");
                break;
                     }
    }

    @Then("^I can see the message: '(.+)'$")
    public void i_can_see_the_following_message(String message) throws Throwable {
switch (message){
    case "Username or Password is incorrect":
        Assert.assertTrue(loginPage.getFailedLoginValidationMessage().contains(message));
        break;
    case "Login Successful":
        Assert.assertTrue(toolsPage.getLoginSuccessfulMessage().contains(message));
        break;
    default:
        Assert.fail("The supplied message data is wrong or doesn't exist");
        break;
}
    }


}
