package com.safebear.auto.tests;
import com.safebear.auto.utils.Utils;
import cucumber.api.CucumberOptions;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.apache.catalina.filters.WebdavFixFilter;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;


@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber"},
//                "html:target/cucumber-reports/cucumber-pretty",
//                "json:target/cucumber-reports/CucumberTestReport.json",
//                "return:target/cucumber-reports/rerun.txt"},
        tags = {"~@to-do"},
        glue = "com.safebear.auto.tests",
        features = "classpath:toolslist.features/login.feature"
)


public class RunCukes extends AbstractTestNGCucumberTests {

//
//    WebDriver driver;
//
//    @BeforeClass
//    public void setUp() {
//        driver = Utils.getDriver();
//    }
//
//    @AfterClass
//    public void tearDown() {
//        try {
//            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "0")));
//        } catch (InteruptedException e) {
//            e.printStackTrace();
//        }
//        driver.quit();
//    }

  }

