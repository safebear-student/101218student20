package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {

    LoginPageLocators locators = new LoginPageLocators();
    private String expectedPageTitle = "Login Page";

    @NonNull
    WebDriver driver;

public boolean isPageTitleAsExpected(){
    return expectedPageTitle == getPageTitle();
}

    public String getPageTitle(){
        return driver.getTitle();

    }

    public String getExpectedPageTitle(){
        return expectedPageTitle;
    }

    public void login(String un, String pw){
        driver.findElement(locators.getUsernameLocator()).sendKeys(un);
        driver.findElement(locators.getPasswordLocator()).sendKeys(pw);
        driver.findElement(locators.getLoginButtonLocator()).click();
 //       driver.findElement(locators.getPasswordLocator()).submit();
    }

    public void enterUsername(String uname){
        driver.findElement(locators.getUsernameLocator()).sendKeys(uname);
    }

    public String getFailedLoginValidationMessage(){
        return driver.findElement(locators.getFailedLoginMessage()).getText();
    }




}
