Feature: Login
  In order to access the website
  As a user
  I want to know if my login is successful

  Rules:
  * User must be informed if login successful
  * User must be informed if login fails

  Glossary:
  User: Someone that wants to create a toolslist

  @HighRisk
  @HighImpact
  Scenario Outline: Navigate and login
    Given I navigate to the login page
    When I enter the login info for '<userType>'
    Then I can see the message: '<validationMessage>'
  Examples:
  | userType    | validationMessage                 |
  | invalidUser | Username or Password is incorrect|
  | validUser   | Login Successful|

